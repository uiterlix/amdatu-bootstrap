package org.amdatu.bootstrap.core;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.bootstrap.core.api.PluginFilter;
import org.amdatu.bootstrap.core.api.PluginRegistry;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;

public class PluginRegistryImpl implements PluginRegistry {
	private final Map<String, PluginRegistration> m_plugins = new ConcurrentHashMap<>();
	private final Map<String, DependencyManager> m_dependencyManagers = new ConcurrentHashMap<>();

	public void pluginAdded(ServiceReference<Plugin> ref, Plugin plugin) {
		
		Properties props = new Properties();
		List<String> commands = new ArrayList<>();
		Method[] declaredMethods = plugin.getClass().getDeclaredMethods();
		for (Method method : declaredMethods) {
			if(method.isAnnotationPresent(Command.class)) {
				commands.add(method.getName());
			}
		}
		
		String[] commandNames = new String[commands.size()];
		props.put("name", plugin.getName());
		props.put(CommandProcessor.COMMAND_SCOPE, plugin.getName());
		props.put(CommandProcessor.COMMAND_FUNCTION, commands.toArray(commandNames));
		
		String symbolicName = ref.getBundle().getSymbolicName();
		if(!m_dependencyManagers.containsKey(symbolicName)) {
			DependencyManager dm = new DependencyManager(ref.getBundle().getBundleContext());
			m_dependencyManagers.put(symbolicName, dm);
		}
		
		DependencyManager dm = m_dependencyManagers.get(symbolicName);
		Component component = dm.createComponent().setInterface(Object.class.getName(), props).setImplementation(plugin);
		
		try {
			dm.add(component);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		m_plugins.put(plugin.getName(), new PluginRegistration(plugin, component));
	}

	public void pluginRemoved(ServiceReference<Plugin> ref, Plugin plugin) {
		String symbolicName = ref.getBundle().getSymbolicName();
		DependencyManager dm = m_dependencyManagers.get(symbolicName);
		
		PluginRegistration pluginRegistration = m_plugins.get(plugin.getName());
		dm.remove(pluginRegistration.getCommand());
		m_plugins.remove(plugin.getName());
	}
	
	public void pluginBundleRemoved(Bundle bundle) {
		if(m_dependencyManagers.containsKey(bundle.getSymbolicName())) {
			DependencyManager dependencyManager = m_dependencyManagers.get(bundle.getSymbolicName());
			dependencyManager.clear();
			
			m_dependencyManagers.remove(bundle.getSymbolicName());
		}
	}

	@Override
	public List<Plugin> listPlugins() {
		List<Plugin> plugins = new ArrayList<>();
		for (PluginRegistration registration : m_plugins.values()) {
			plugins.add(registration.getPlugin());
		}
		
		return plugins;
	}

	@Override
	public Plugin getPlugin(String name) {
		return m_plugins.get(name).getPlugin();
	}

	@Override
	public List<Plugin> listPlugins(PluginFilter filter) {
		List<Plugin> plugins = new ArrayList<>();
		for (PluginRegistration registration : m_plugins.values()) {
			if(filter.filter(registration.getPlugin())) {
				plugins.add(registration.getPlugin());
			}
		}
		
		return plugins;
	}

}
