package org.amdatu.bootstrap.core;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.PluginFilter;
import org.amdatu.bootstrap.core.api.PluginRegistry;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Dependency;
import org.amdatu.bootstrap.plugin.api.Plugin;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.VersionedClause;
import aQute.bnd.header.Attrs;
import aQute.bnd.properties.Document;

public class CoreCommandsImpl extends AbstractBasePlugin {

	private volatile PluginRegistry m_pluginRegistry;
	private volatile Navigator m_navigator;
	private volatile Prompt m_prompt;
	private volatile ResourceManager m_resourceManager;
	private volatile DependencyBuilder m_dependencyBuilder;

	@Command(description="List all registered plugins")
	public void listPlugins() {
		List<Plugin> listPlugins = m_pluginRegistry.listPlugins();
		for (Plugin plugin : listPlugins) {
			System.out.println(plugin.getName());
		}
	}

	@Command(description="Show commands for a plugin")
	public void man(String... pluginName) {
	    if (pluginName.length == 0) {
	        System.out.println("Please enter a name of a plugin to show the commands");
	        return;
	    }
		Plugin plugin = m_pluginRegistry.getPlugin(pluginName[0]);
		
		Method[] declaredMethods = plugin.getClass().getDeclaredMethods();
		for (Method method : declaredMethods) {
			if(method.isAnnotationPresent(Command.class)) {
				Command command = method.getAnnotation(Command.class);
				System.out.println(method.getName() + " - " + command.description());
			}
		}
	}
	
	@Command
	public void cd(String dirName) {
		Path currentDir = m_navigator.getCurrentDir();
		Path newDir;
		
		if(dirName.equals("..")) {
			newDir = currentDir.getParent();
		} else {
			newDir = currentDir.resolve(dirName);
		}
		
		if(newDir.toFile().exists() && newDir.toFile().isDirectory()) {
			m_navigator.changeDir(newDir);
		} else {
			System.out.println("Invalid directory " + newDir);
		}
	}
	
	@Command
	public void pwd() {
		System.out.println(m_navigator.getCurrentDir());
	}
	
	@Command
	public void ls() {
		File[] listFiles = m_navigator.getCurrentDir().toFile().listFiles();
		for (File file : listFiles) {
			System.out.println(file.getName());
		}
	}
	
	@Command(description = "Creates a folder in the current directory")
    public void mkdir(String folderName) {
	    String pathname = m_navigator.getCurrentDir().toString() + "/" + folderName;
        new File(pathname).mkdirs();
        System.out.println("Folder " + pathname + " created");
    }
	
	@Command
	public void createProject() {
		String name = m_prompt.askString("Name of the project?");
		m_navigator.createProject(name);
	}
	
	@Command
	public void install() throws Exception {
		install(null);
	}
	
	@Command
	public void install(String name) throws Exception {
	    Path workspaceDir = m_navigator.getWorkspaceDir();
	    if (workspaceDir == null) {
	        System.out.println("You can only install plugins in a workspace or a project");
	        return;
	    }
	    final boolean isProject = m_navigator.getProjectDir() != null;
		List<Plugin> plugins = m_pluginRegistry.listPlugins(new PluginFilter() {
			@Override
			public boolean filter(Plugin plugin) {
				return ((isProject && plugin.isProjectInstallAllowed()) || (plugin.isWorkspaceInstallAllowed())) 
				                && !plugin.isInstalled();
			}
		});
		
		if (plugins.isEmpty()) {
		    System.out.println("Nothing to install at this level at this time");
		    return;
		}
		
		Plugin pluginToInstall;
		if(name != null) {
			pluginToInstall = m_pluginRegistry.getPlugin(name);
		} else {
			pluginToInstall = m_prompt.askChoice("Which plugin do you want to install?", 0, plugins);
		}
		
		Collection<Dependency> dependencies = pluginToInstall.getDependencies();
		
		m_dependencyBuilder.addDependencies(dependencies);
		
		pluginToInstall.install();
	}
	
	@Command
	public void addRunConfig() throws IOException {
		List<Plugin> plugins = m_pluginRegistry.listPlugins();
		Plugin pluginToInstall = m_prompt.askChoice("Which run requirements do you want to install?", 0, plugins);
		
		Collection<Dependency> runRequirements = pluginToInstall.getRunRequirements();
		Path path = m_prompt.askChoice("Which run config do you want to add to?", 0, m_navigator.findWorkspaceRunConfigs());
		
		BndEditModel model = new BndEditModel(); 
		
		model.loadFrom(path.toFile());
		
		List<VersionedClause> runBundles = model.getRunBundles();
		if(runBundles == null) {
			runBundles = new ArrayList<>();
		}
		
		for (Dependency dependency : runRequirements) {
			addRunBundle(dependency, runBundles);
		}
		
		model.setRunBundles(runBundles);
		
		String bndContents = new String(Files.readAllBytes(path));
		Document document = new Document(bndContents);
		model.saveChangesTo(document);
		
		m_resourceManager.writeFile(path, document.get().getBytes());
	}
	
	@Command
	public void listInstalled() {
	    Path workspaceDir = m_navigator.getWorkspaceDir();
        if (workspaceDir == null) {
            System.out.println("You can only install plugins in a workspace or a project");
            return;
        }
        final boolean isProject = m_navigator.getProjectDir() != null;
		List<Plugin> plugins = m_pluginRegistry.listPlugins(new PluginFilter() {
			@Override
			public boolean filter(Plugin plugin) {
			    return ((isProject && plugin.isProjectInstallAllowed()) || (plugin.isWorkspaceInstallAllowed())) 
                                && plugin.isInstalled();
			}
		});
		
		for (Plugin plugin : plugins) {
			System.out.println(plugin.getName());
		}
	}
	
	private void addRunBundle(Dependency dependency, List<VersionedClause> runBundles) {
		for (VersionedClause versionedClause : runBundles) {
			if(versionedClause.getName().equals(dependency.getBsn())) {
				System.out.println("Skipping " + dependency.getBsn() + " since it was already in the run config");
				return;
			}
		}
		
		runBundles.add(new VersionedClause(dependency.getBsn(), new Attrs()));		
	}
	
	@Override
	public String getName() {
		return "core";
	}
	
	@Override
	public boolean isInstalled() {
		return true;
	}
	
}
