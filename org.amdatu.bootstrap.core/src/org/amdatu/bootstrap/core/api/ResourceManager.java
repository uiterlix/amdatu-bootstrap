package org.amdatu.bootstrap.core.api;

import java.io.File;
import java.nio.file.Path;

public interface ResourceManager {
	void writeFile(Path path, byte[] contents);
	/**
	 * Unzips a file to a folder
	 * @param fileToUnzip the file to unzip
	 * @param toLocation a location to store the contents of the zip file. This must be a folder.
	 */
	void unzipFile(File fileToUnzip, File toLocation);
	
	/** 
	 * Removes a file or directory, if the directory contains files all files will be deleted as well.
	 */
	void delete(File fileToDelete);
}
