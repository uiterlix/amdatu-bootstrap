package org.amdatu.bootstrap.core.api;

import org.amdatu.bootstrap.plugin.api.Plugin;

public interface PluginFilter {
	boolean filter(Plugin plugin);
}
