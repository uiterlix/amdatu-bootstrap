package org.amdatu.bootstrap.core.api;

import java.util.Collection;
import java.util.List;

import org.amdatu.bootstrap.plugin.api.Dependency;

public interface DependencyBuilder {

	void addDependency(String bsn);

	void addDependency(String bsn, String version);

	/** This will add a Dependency to the bnd build path of the current project. */
	void addDependency(Dependency dependency);

	void addDependencies(Collection<Dependency> dependencies);
	
	/** Will call {@link #addRunDependency(String, String)} with version null */
	void addRunDependency(String bsn);

	/** Will call {@link #addRunDependency(Dependency)} with a Dependency constructed from the bsn and version. */
    void addRunDependency(String bsn, String version);

    /** Will call {@link #addRunDependency(Collection)} with a list of 1 {@link Dependency}.  */
    void addRunDependency(Dependency dependency);

    /**
     * This will add a (list of) {@link Dependency} to the bnd run bundles of the current project. 
     * It will update if the {@link Dependency} already exits. 
     */
    void addRunDependency(Collection<Dependency> dependencies);

	boolean hasDependency(Dependency dependency);

	boolean hasDependency(String bsn, String version);

	boolean hasDependency(String bsn);
	
	boolean hasDependencies(Collection<Dependency> dependencies);
	
	/** This will return all build and run Dependencies of the current project. */
	List<Dependency> listDependencies();

	void updateDependency(String bsn);
	
    void updateDependency(String bsn, String version);
    
    /** This will update a build and run {@link Dependency} of the current project. */
    void updateDependency(Dependency dependency);
}