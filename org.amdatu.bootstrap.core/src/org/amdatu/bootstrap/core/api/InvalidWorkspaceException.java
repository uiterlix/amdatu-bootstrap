package org.amdatu.bootstrap.core.api;

public class InvalidWorkspaceException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public InvalidWorkspaceException(Exception ex) {
		super(ex);
	}
}
