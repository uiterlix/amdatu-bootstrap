package org.amdatu.bootstrap.core.api;

public class InvalidProjectException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public InvalidProjectException(Exception e) {
		super(e);
	}

}
