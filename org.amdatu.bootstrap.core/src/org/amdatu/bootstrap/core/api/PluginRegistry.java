package org.amdatu.bootstrap.core.api;

import java.util.List;

import org.amdatu.bootstrap.plugin.api.Plugin;

public interface PluginRegistry {
	List<Plugin> listPlugins();
	List<Plugin> listPlugins(PluginFilter filter);
	Plugin getPlugin(String name);
}
