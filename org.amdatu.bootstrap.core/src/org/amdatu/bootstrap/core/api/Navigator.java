package org.amdatu.bootstrap.core.api;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

import aQute.bnd.build.Project;
import aQute.bnd.build.Workspace;

public interface Navigator {

	Workspace getCurrentWorkspace();

	Project getCurrentProject();

	Path getCurrentDir();

	void changeDir(Path newDir);

	Path getProjectDir();

	Path getWorkspaceDir();

	void createProject(String name);

	Path getBndFile();
	
    List<Path> getBndRunFiles();
	
	List<File> listProjectBndFiles(); 
	
	List<Path> findWorkspaceRunConfigs();

}