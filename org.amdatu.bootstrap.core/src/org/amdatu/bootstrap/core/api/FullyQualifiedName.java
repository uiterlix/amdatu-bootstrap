package org.amdatu.bootstrap.core.api;

public class FullyQualifiedName {
	private final String className;
	private final String packageName;
	
	public FullyQualifiedName(String name) {
		if(name.contains(".")) {
			packageName = name.substring(0, name.lastIndexOf("."));
			className = name.substring(name.lastIndexOf(".") + 1);
		} else {
			className = name;
			packageName = null;
		}
	}
	
	public String getClassName() {
		return className;
	}
	public String getPackageName() {
		return packageName;
	}

	public String getFull() {
		return packageName + "." + className;
	}
}
