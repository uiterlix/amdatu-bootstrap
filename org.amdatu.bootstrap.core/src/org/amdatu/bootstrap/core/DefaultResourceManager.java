package org.amdatu.bootstrap.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.amdatu.bootstrap.core.api.ResourceManager;

public class DefaultResourceManager implements ResourceManager {
    @Override
    public void writeFile(Path path, byte[] contents) {
        try {
            Files.write(path, contents, StandardOpenOption.TRUNCATE_EXISTING);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void unzipFile(File fileToUnzip, File toLocation) {
        byte[] buffer = new byte[1024];

        // create output directory is not exists
        if (!toLocation.exists()) {
            toLocation.mkdir();
        }
        // get the zip file content
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(fileToUnzip))) {
            // get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {

                String fileName = ze.getName();
                File newFile = new File(toLocation + File.separator + fileName);

                // create all non exists folders
                // else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                if (ze.isDirectory()) {
                    // create the folder
                    newFile.mkdirs();
                }
                else {
                    // extract file
                    try (FileOutputStream fos = new FileOutputStream(newFile)) {
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                        }
                    }
                }

                ze = zis.getNextEntry();
            }

            zis.closeEntry();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(File f) {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                delete(c);
            }
        }
        if (!f.delete()) {
            throw new RuntimeException("Failed to delete file: " + f);
        }
    }
}
