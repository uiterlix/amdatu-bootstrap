package org.amdatu.bootstrap.core;

import org.amdatu.bootstrap.plugin.api.Plugin;
import org.apache.felix.dm.Component;

public class PluginRegistration {
	private final Plugin m_plugin;
	private final Component m_command;
	
	public PluginRegistration(Plugin plugin, Component command) {
		m_plugin = plugin;
		m_command = command;
	}

	public Plugin getPlugin() {
		return m_plugin;
	}

	public Component getCommand() {
		return m_command;
	}
}
