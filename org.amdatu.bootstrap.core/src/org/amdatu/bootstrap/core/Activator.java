package org.amdatu.bootstrap.core;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.PluginRegistry;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.template.processor.TemplateEngine;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventAdmin;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(BundleContext ctx, DependencyManager dm) throws Exception {

		dm.add(createComponent()
				.setInterface(PluginRegistry.class.getName(), null)
				.setImplementation(PluginRegistryImpl.class)
				.add(createServiceDependency().setService(Plugin.class)
						.setCallbacks("pluginAdded", "pluginRemoved"))
				.add(createBundleDependency().setCallbacks("pluginBundleAdded",
						"pluginBundleRemoved")));
		
		dm.add(createComponent()
				.setInterface(Navigator.class.getName(), null)
				.setImplementation(NavigatorImpl.class)
				.add(createServiceDependency().setService(EventAdmin.class)
						.setRequired(true))
				.add(createServiceDependency().setService(TemplateEngine.class)
						.setRequired(true)));

		dm.add(createComponent()
				.setInterface(DependencyBuilder.class.getName(), null)
				.setImplementation(DependencyBuilderImpl.class)
				.add(createServiceDependency().setService(Navigator.class)
						.setRequired(true))
				.add(createServiceDependency()
						.setService(ResourceManager.class).setRequired(true))
				.add(createServiceDependency().setService(Prompt.class)
						.setRequired(true)));

		dm.add(createComponent().setInterface(ResourceManager.class.getName(),
				null).setImplementation(DefaultResourceManager.class));
		
		dm.add(createComponent()
				.setInterface(Plugin.class.getName(), null)
				.setImplementation(CoreCommandsImpl.class)
				.add(createServiceDependency().setService(PluginRegistry.class))
				.add(createServiceDependency().setService(Navigator.class))
				.add(createServiceDependency().setService(Prompt.class))
				.add(createServiceDependency().setService(ResourceManager.class).setRequired(true))
				.add(createServiceDependency().setService(DependencyBuilder.class).setRequired(true)));
	}

}
