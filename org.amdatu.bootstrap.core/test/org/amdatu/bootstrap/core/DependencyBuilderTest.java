package org.amdatu.bootstrap.core;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DependencyBuilderTest {
	@InjectMocks @Spy private DependencyBuilder m_dependencyBuilder = new DependencyBuilderImpl();
	@Mock private Navigator m_navigator;
	@Mock private ResourceManager m_resourceManager;
	@Captor private ArgumentCaptor<byte[]> contents; 
	
	@Before
	public void setup() throws URISyntaxException {
		URL resource = getClass().getClassLoader().getResource("bnd.bnd.test");
		when(m_navigator.getBndFile()).thenReturn(Paths.get(resource.toURI()));
	}
	
	@Test
	public void testAddDependencyWithVersion() {
		m_dependencyBuilder.addDependency("org.amdatu.test", "latest");
		
		verify(m_resourceManager).writeFile(any(Path.class), contents.capture());
		assertTrue(new String(contents.getValue()).contains("org.amdatu.test;version=latest"));
	}
	
	@Test
	public void testAddDependencyWithVersionRange() {
		m_dependencyBuilder.addDependency("org.amdatu.test", "[1.0.0,2.0.0)");
		
		verify(m_resourceManager).writeFile(any(Path.class), contents.capture());
		assertTrue(new String(contents.getValue()).contains("org.amdatu.test;version='[1.0.0,2.0.0)'"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddDependencyWithInvalidVersionRange() {
		m_dependencyBuilder.addDependency("org.amdatu.test", "[1.0.0");
		verify(m_resourceManager).writeFile(any(Path.class), contents.capture());
	}
	
	@Test
	public void testAddDependencyWithoutVersion() {
		m_dependencyBuilder.addDependency("org.amdatu.test");
		
		verify(m_resourceManager).writeFile(any(Path.class), contents.capture());
		assertTrue(new String(contents.getValue()).contains("org.amdatu.test"));
	}

}
