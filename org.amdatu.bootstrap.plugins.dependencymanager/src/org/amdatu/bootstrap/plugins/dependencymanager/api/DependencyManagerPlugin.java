package org.amdatu.bootstrap.plugins.dependencymanager.api;

import org.amdatu.bootstrap.core.api.FullyQualifiedName;

public interface DependencyManagerPlugin {
	void createActivator(FullyQualifiedName fqn, FullyQualifiedName interfaceName, ServiceDependencyDescription... dependencies);
}
