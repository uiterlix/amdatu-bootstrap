package org.amdatu.bootstrap.plugins.dependencymanager.api;

import org.amdatu.bootstrap.core.api.FullyQualifiedName;

public class ServiceDependencyDescription {
	private final FullyQualifiedName m_fqn;
	private final boolean m_required;

	public ServiceDependencyDescription(String fqn, boolean required) {
		m_fqn = new FullyQualifiedName(fqn);
		m_required = required;
	}

	public ServiceDependencyDescription(String fqn) {
		m_fqn = new FullyQualifiedName(fqn);
		m_required = false;
	}

	public FullyQualifiedName getFqn() {
		return m_fqn;
	}

	public boolean isRequired() {
		return m_required;
	}

	@Override
	public String toString() {
		return "ServiceDependency [m_fqn=" + m_fqn + ", m_required=" + m_required + "]";
	}
}
