package org.amdatu.bootstrap.plugins.baseline;

import java.io.File;
import java.nio.file.Files;
import java.util.Collection;
import java.util.List;

import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.Version;

import aQute.bnd.build.Project;
import aQute.bnd.build.ProjectBuilder;
import aQute.bnd.build.Workspace;
import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.differ.Baseline.BundleInfo;
import aQute.bnd.differ.Baseline.Info;
import aQute.bnd.properties.Document;
import aQute.lib.io.IO;
import aQute.service.reporter.Report.Location;

@Component(provides=Plugin.class)
public class BaselinePlugin extends AbstractBasePlugin{

	@ServiceDependency
	private Navigator m_navigator;
	
	@ServiceDependency 
	private ResourceManager m_resourceManager;
	
	@Override
	public String getName() {
		return "baseline";
	}

	@Override
	public boolean isInstalled() {
		return true;
	}

	@Command
	public void check() throws Exception {
		Workspace currentWorkspace = m_navigator.getCurrentWorkspace();
		Collection<Project> buildorder = currentWorkspace.getBuildOrder();
		
		for(Project project : buildorder) {
			
			checkProject(project);
			
		}
		
		
	}

	private void checkProject(Project project) throws Exception {
		project.clear();
		
		project.clean();
		project.build();
		
		List<String> errors = project.getErrors();
		
		
		for (String string : errors) {
			if(string.matches(".*version [0-9\\.]+ is too low.*")) {
				Location location = project.getLocation(string);
				if(location.details instanceof BundleInfo) {
					BundleInfo bundleInfo = (BundleInfo)location.details;
					System.out.println("Updated bundle: " + bundleInfo.bsn + " from version " + bundleInfo.version + " to version " + bundleInfo.suggestedVersion);
					
					BndEditModel model = new BndEditModel();
					File bndFile = new File(location.file);
					model.loadFrom(bndFile);
					
					model.setBundleVersion(bundleInfo.suggestedVersion.toString());
					
					Document document = new Document(new String(Files.readAllBytes(bndFile.toPath())));
					model.saveChangesTo(document);
					
					String documentContents = document.get();
					m_resourceManager.writeFile(bndFile.toPath(), documentContents.getBytes());
				}
			} else if(string.matches(".*Baseline mismatch for package.*")) {
				Location location = project.getLocation(string);
				if(location.details instanceof Info) {
					Info info = (Info)location.details;
					
					System.out.println("Package: " + info.packageName + " should have version " + info.suggestedVersion + ". Please change manually, Bootstrap doesn't support updating package versions yet.");
				}
			}
		}
		
	}
}
