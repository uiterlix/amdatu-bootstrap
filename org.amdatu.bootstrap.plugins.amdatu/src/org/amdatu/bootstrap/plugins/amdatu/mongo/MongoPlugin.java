package org.amdatu.bootstrap.plugins.amdatu.mongo;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.FullyQualifiedName;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Dependency;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.dependencymanager.api.DependencyManagerPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.api.ServiceDependencyDescription;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component(provides = Plugin.class)
public class MongoPlugin extends AbstractBasePlugin {
	@ServiceDependency
	private Prompt m_prompt;

	@ServiceDependency
	private DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private TemplateEngine m_templateEngine;
	
	@ServiceDependency
	private Navigator m_navigator;
	
	@ServiceDependency
	private DependencyManagerPlugin m_dependencyManagerPlugin;
	
	private BundleContext m_bundleContext;
	
	@Override
	public void install() throws Exception {
		int mapperIndex = m_prompt.askChoiceAsIndex("Which object mapping framework do you want to use?", 0,
				Arrays.asList("Mongo Jackson Mapper", "Jongo", "None"));
		
		switch (mapperIndex) {
		case 0:
			installJacksonMapper();
			break;
			
		case 1: 
			installJongo();
			break;
		}
	}

	private void installJacksonMapper() {
		m_dependencyBuilder.addDependencies(Dependency.fromStrings(
				"net.vz.mongodb.jackson.mongo-jackson-mapper",
				"jackson-core-asl",
				"jackson-mapper-asl"));
	}

	private void installJongo() {
		m_dependencyBuilder.addDependencies(Dependency.fromStrings(
				"org.jongo",
				"de.undercouch.bson4jackson"));
	}

	@Command
	public void addComponent() {
		String registrationType = m_prompt.askChoice("How do you want to register the component?", 0, Arrays.asList("Annotations", "Activator"));
		FullyQualifiedName fqn = m_prompt.askComponentName();
		String collection = m_prompt.askString("What collection in Mongo do you want to use?");
		FullyQualifiedName interfaceName = m_prompt.askComponentName("Which interface do you want to implement?");
		
		boolean useAnnotations = registrationType.equals("Annotations");
		FullyQualifiedName objectType = null;
		
		URL templateUri;
		if(m_dependencyBuilder.hasDependency("org.jongo")) {
			templateUri = m_bundleContext.getBundle().getEntry("/templates/mongo-jongocomponent.vm");
		} else if(m_dependencyBuilder.hasDependency("net.vz.mongodb.jackson.mongo-jackson-mapper")) {
			templateUri = m_bundleContext.getBundle().getEntry("/templates/mongo-jacksoncomponent.vm");
			objectType = m_prompt.askComponentName("What is the type of object that you want to map?");
		} else {
			templateUri = m_bundleContext.getBundle().getEntry("/templates/mongo-nomappingcomponent.vm");
		}
		
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("componentName", fqn.getClassName());
			context.put("packageName", fqn.getPackageName());
			context.put("useAnnotations", useAnnotations);
			context.put("collection", collection);
			context.put("objectType", objectType);
			context.put("interfaceName", interfaceName);
			
			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("src").resolve(fqn.getPackageName().replaceAll("\\.", "/"));
			Path outputFile = outputDir.resolve(fqn.getClassName() + ".java");
			Files.createDirectories(outputDir);
			
			System.out.println("Created file: " + outputFile);
			Files.write(outputFile, template.getBytes());
			
		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
		
		
		if(!useAnnotations) {
			m_dependencyManagerPlugin.createActivator(fqn, interfaceName, new ServiceDependencyDescription("org.amdatu.mongo.MongoDBService", true));
		} 
		
	}
	
	
	@Override
	public String getName() {
		return "mongo";
	}

	@Override
	public Collection<Dependency> getDependencies() {
		return Dependency.fromStrings("org.mongodb.mongo-java-driver", "org.amdatu.mongo");
	}

	@Override
	public Collection<Dependency> getRunRequirements() {
		List<Dependency> dependencies = Dependency.fromStrings(
				"org.mongodb.mongo-java-driver",
 			    "org.amdatu.mongo",
 			    "org.apache.felix.configadmin");
		
		if(m_dependencyBuilder.hasDependency("org.jongo")) {
			dependencies.addAll( Dependency.fromStrings(
					"com.fasterxml.jackson.core.jackson-core",
					"com.fasterxml.jackson.core.jackson-databind",
					"com.fasterxml.jackson.core.jackson-annotations",
					"de.undercouch.bson4jackson",
					"org.jongo"));
		}
		
		if(m_dependencyBuilder.hasDependency("net.vz.mongodb.jackson.mongo-jackson-mapper")) {
			dependencies.addAll( Dependency.fromStrings(
					"de.undercouch.bson4jackson",
					"net.vz.mongodb.jackson.mongo-jackson-mapper",
					"jackson-core-asl",
					"jackson-mapper-asl"));
		}
		return dependencies;
	}
	
	@Override
	public boolean isInstalled() {
		// TODO Auto-generated method stub
		return false;
	}

}
