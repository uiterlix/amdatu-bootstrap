package org.amdatu.bootstrap.plugins.amdatu.web.api;

import java.io.File;

public interface WebResourcePlugin {

	void addWebResources(String path, String defaultPath, String resourcesDir, File bndFile);

}
