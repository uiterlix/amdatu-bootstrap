package org.amdatu.bootstrap.plugins.navigator;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component(provides = Plugin.class)
public class ShellNavigator extends AbstractBasePlugin {
    
    @ServiceDependency
    private Prompt m_prompt;
    
    @ServiceDependency
    private Navigator m_navigator;

    @Override
    public String getName() {
        return "ShellNavigator";
    }

    @Override
    public boolean isInstalled() {
        return true;
    }
    
    @Command(description = "short hand for navigate")
    public void nav() {
        navigate();
    }
    
    @Command(description = "a shell frendly navigator, it prompts you on what folder to move to")
    public void navigate() {
        while (true) {
            System.out.println("In " + m_navigator.getCurrentDir());
            File[] listFiles = m_navigator.getCurrentDir().toFile().listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.isDirectory();
                }
            });
            List<File> options = new ArrayList<>();
            options.add( m_navigator.getCurrentDir().getParent().toFile());
            options.addAll(Arrays.asList(listFiles));
            File choice = m_prompt.askChoice("Navigate to what dir? (no option to exit)", -1, options);
            if (choice == null) {
                // user wants to exit
                return;
            }
            m_navigator.changeDir(choice.toPath());
        }
    }

}
