package org.amdatu.bootstrap.plugins.version;

import java.io.File;
import java.lang.reflect.Constructor;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Dependency;
import org.amdatu.bootstrap.plugins.version.api.VersionService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.HeaderClause;
import aQute.bnd.service.RepositoryPlugin;
import aQute.bnd.version.Version;

@Component(provides = VersionService.class)
public class VersionServiceImpl implements VersionService {
    
    @ServiceDependency
    private volatile Navigator m_navigator;
    
    @ServiceDependency
    private volatile DependencyBuilder m_dependencyBuilder;
    
    @ServiceDependency
    private volatile EventAdmin m_eventAdmin;
    
    private Map<String, List<RepositoryPlugin>> m_loadedRepos = new HashMap<>();

    @Command
    public SortedSet<Version> findVersion(String bsn) {
        if (bsn == null) {
            // option was not valid exit..
            return new TreeSet<>();
        }
        List<RepositoryPlugin> repositories = getRepositories();
        if (repositories == null) {
            // Short circuit and exit we can't do anything
        	System.out.println("No repositories found");
        	return null;
        }
        SortedSet<Version> versions = new TreeSet<>();
        for (RepositoryPlugin r: repositories) {
            try {
                SortedSet<Version> sortedSet = r.versions(bsn);
                versions.addAll(sortedSet);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return versions;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<RepositoryPlugin> getRepositories(){
        Path workspaceDir = m_navigator.getWorkspaceDir();
        if (workspaceDir == null) {
            System.out.println("Need to be in a workspace or project to execute this");
            return null;
        }
        if (m_loadedRepos.containsKey(workspaceDir.toString())) {
            // We can load it from cache
            return m_loadedRepos.get(workspaceDir.toString());
        }
        // clear old repo's
        m_loadedRepos.clear();
        
        BndEditModel model = new BndEditModel();
        File bndFile = new File(workspaceDir.toString() + "/cnf/ext/repositories.bnd");
        
        try {
            model.loadFrom(bndFile);
            List<HeaderClause> plugins = model.getPlugins();
            List<RepositoryPlugin> repos = new ArrayList<>();
            for (HeaderClause hc : plugins) {
                try {
                    Class<RepositoryPlugin> clazz = (Class<RepositoryPlugin>) Class.forName(hc.getName());
                    Constructor<?> ctor = clazz.getConstructor();
                    RepositoryPlugin r = (RepositoryPlugin) ctor.newInstance();
                    
                    Map<String, String> map = new HashMap<>();
                    for (String kv : hc.getAttribs().toString().split(";")) {
                        String[] split = kv.split("=");
                        String value = split[1];
                        if (value.startsWith("\"") && value.endsWith("\"")) {
                            value = value.substring(1, value.length() -1);
                        }
                        // Set current workspace folder
                        value = value.replace("${workspace}", workspaceDir.toString());
                        map.put(split[0], value);
                    }
                    if (r instanceof aQute.bnd.service.Plugin) {
                        ((aQute.bnd.service.Plugin)r).setProperties(map);
                    }
                    
                    repos.add(r);
                    
                } catch (ClassNotFoundException ce) {
                    System.out.println("Can't load repository with class: '" + hc.getName() + "' index might not be complete!");
                }
            }
            
            m_loadedRepos.put(workspaceDir.toString(), repos);
            
            return repos;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public Map<String, Set<String>> getAllDependencies() {
        sendEvent("org/amdatu/bootstrap/core/BE_QUIET");
        Path workspaceDir = m_navigator.getWorkspaceDir();
        if (workspaceDir == null) {
            System.out.println("Need to be in a workspace or project to execute this");
            return new HashMap<>();
        }
        
        Path currentDir = m_navigator.getCurrentDir();
        
        Path projectDir = m_navigator.getProjectDir();
        List<File> projects = new ArrayList<>();
        if (projectDir == null && !m_navigator.getBndRunFiles().isEmpty()) {
            projects.add(m_navigator.getCurrentDir().toFile());
        } else if (projectDir == null) {
            for (File f : workspaceDir.toFile().listFiles()) {
                if (f.isDirectory()) {
                    projects.add(f);
                }
            }
        } else {
            projects.add(projectDir.toFile());
            currentDir = projectDir;
        }
        
        Map<String, Set<String>> allDeps = new HashMap<>();
        
        for (File f : projects) {
            try {
                changeToDir(f.toPath());
                
                List<Dependency> list = m_dependencyBuilder.listDependencies();
                for (Dependency d : list) {
                    Set<String> versions = allDeps.get(d.getBsn());
                    if (versions == null) {
                        versions = new HashSet<>();
                        allDeps.put(d.getBsn(), versions);
                    }
                    versions.add(d.getVersion());
                }
            } catch (RuntimeException e) {
                // Probably not a correct folder ignore
            }
        }
        
        // reset to old dir
        changeToDir(currentDir);
        sendEvent("org/amdatu/bootstrap/core/RESET_QUIET");
        return allDeps;
    }
    
    private void changeToDir(Path to) {
        Path currentDir = m_navigator.getCurrentDir();
        if (!currentDir.equals(to)) {
            m_navigator.changeDir(to);
        }
    }
    
    private void sendEvent(String topicName) {
        Map<String, Object> props = new HashMap<>();
        Event event = new Event(topicName, props);
        m_eventAdmin.sendEvent(event);
    }

}
