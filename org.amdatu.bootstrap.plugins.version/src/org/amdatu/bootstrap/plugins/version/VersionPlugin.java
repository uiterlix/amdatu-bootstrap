package org.amdatu.bootstrap.plugins.version;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.version.api.VersionService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import aQute.bnd.service.RepositoryPlugin;
import aQute.bnd.version.Version;

@Component(provides = Plugin.class)
public class VersionPlugin extends AbstractBasePlugin {
    
    @ServiceDependency
    private VersionService m_versionService;
    
    @ServiceDependency
    private Prompt m_prompt;
    
    @ServiceDependency
    private Navigator m_navigator;
    
    @ServiceDependency
    private DependencyBuilder m_dependencyBuilder;

    @ServiceDependency
    private volatile EventAdmin m_eventAdmin;

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public boolean isInstalled() {
        return false;
    }
    
    @Command(description="Checks all repositories for a bsn and lists the versions available")
    public void findVersion() {
        List<RepositoryPlugin> repositories = m_versionService.getRepositories();
        if (repositories == null) {
            // Sort circuit and exit we can't do anything
        }
        String bsn = m_prompt.askString("What is the bsn?");
        if ("".equals(bsn.trim())) {
            System.out.println("Need a bsn to look for a version");
            return;
        }
        SortedSet<Version> versions = m_versionService.findVersion(bsn);
        if (versions.isEmpty()) {
            System.out.println("No versions found (is bsn correct)?");
        } else {
            System.out.println("Found the following versions for bsn: " + bsn);
            for (Version v : versions) {
                System.out.println(v.toString());
            }
        }
    }
    
    @Command(description="Upgrades a build dependecy for a bsn to a specific version")
    public void upgradeVersion() {
        Map<String, Set<String>> allDeps = m_versionService.getAllDependencies();
        upgradeVersion(new ArrayList<String>(allDeps.keySet()));
    }
    
    @Command(description="Upgrades all build dependecies for all projects in a workspace or a single project. Use with care!")
    public void upgradeAllVersions() {
        Map<String, Set<String>> allDeps = m_versionService.getAllDependencies();
        List<String> upgradeble = listAllUpgradableDependecies(allDeps, "-nolist");
        boolean cont = m_prompt.askBoolean("Are you shure you want to continue?", false);
        if (cont) {
            for (String choice : upgradeble) {
                SortedSet<Version> findVersion = m_versionService.findVersion(choice);
                if (!findVersion.isEmpty()) {
                    upgradeVersion(choice, findVersion.last().toString());
                }
            }
        }
    }
    
    @Command(description="Upgrades a build dependecy for a specific bsn to a specific version")
    public String upgradeVersion(String bsn) {
        SortedSet<Version> findVersion = m_versionService.findVersion(bsn);
        List<String> options = new ArrayList<>();
        options.add("null");
        options.add("latest");
        for (Version v : findVersion) {
            options.add(v.toString());
        }
        
        String toVersion = m_prompt.askChoice("To what version do you want to upgrade '" + bsn + "' to?", options.size() -1, options);
       
        upgradeVersion(bsn, toVersion);
        return bsn;
    }
    
    public String upgradeVersion(List<String> bsns) {
        String choice = m_prompt.askChoice("What bsn do you want to upgrade?", -1, bsns);
        if (choice == null) {
            return null;
        }
        return upgradeVersion(choice);
    }

    private void upgradeVersion(String bsn, String toVersion) {
        if (bsn == null || toVersion == null) {
            // do nothing as bsn an version must be set
            return;
        }
        System.out.println("Upgrading " + bsn + " to " + toVersion);
        
        Path projectDir = m_navigator.getProjectDir();
        if (projectDir == null && !m_navigator.getBndRunFiles().isEmpty()) {
            m_dependencyBuilder.updateDependency(bsn, toVersion);
        } else if (projectDir != null) {
            m_dependencyBuilder.updateDependency(bsn, toVersion);
        } else {
            
            List<File> projects = new ArrayList<>();
            for (File f : m_navigator.getWorkspaceDir().toFile().listFiles()) {
                if (f.isDirectory()) {
                    projects.add(f);
                }
            }
            for (File f : projects) {
                try {
                    changeDir(f);
                    Map<String, Set<String>> dependecies = m_versionService.getAllDependencies();
                    if (dependecies.containsKey(bsn)) {
                        m_dependencyBuilder.updateDependency(bsn, toVersion);
                    }
                } catch (RuntimeException e) {
                    // Probably not a correct folder ignore
                }
            }
            // back to workspace
            changeDir(m_navigator.getWorkspaceDir());
        }
    }
    
    @Command(description="Checks if any dependenies in the workspace/project need a update, will also list all version numbers used. Can also take -nolist as argument and will only list upgrades, or -start to automaticly start the upgrade process")
    public void checkUpdates(String... args) {
        Map<String, Set<String>> allDeps = m_versionService.getAllDependencies();
        if (allDeps.isEmpty()) {
            // Nothing more to do... as there are no dependencies
            return;
        }
        if (args.length > 0 && !args[0].startsWith("-")) {
            // apply filter over deps
            Map<String, Set<String>> allDepsFilterd = new HashMap<>();
            for (Entry<String, Set<String>> e : allDeps.entrySet()) {
                if (e.getKey().contains(args[0])) {
                    allDepsFilterd.put(e.getKey(), e.getValue());
                }
            }
            allDeps = allDepsFilterd;
        }
        List<String> upgradeble = listAllUpgradableDependecies(allDeps, args);
        if (isArgsCheck("-start", args)) {
            String lastUpdate = "";
            while (lastUpdate != null && upgradeble.size() > 1) { 
                lastUpdate = upgradeVersion(upgradeble);
                upgradeble.remove(lastUpdate);
                if (!"".equals(lastUpdate)) {
                    boolean another = m_prompt.askBoolean("Upgrade another?", true);
                    if (!another) {
                        return;
                    }
                }
            }
        }
    }
    
    private boolean isArgsCheck(String what, String...args) {
        for (String s : args) {
            if (s.endsWith(what)) {
                return true;
            }
        }
        return false;
    }

    private List<String> listAllUpgradableDependecies(Map<String, Set<String>> allDeps, String... args) {
        // init the repositories, this way any messages will be at the front off the log
        m_versionService.getRepositories();

        // first we need the max length of the longest bundle name;
        int maxLength = getMaxLengthOfName(allDeps);
        List<String> upgradeble = new ArrayList<>();
        if (allDeps.isEmpty()) {
            System.out.println("Nothing found");
            return upgradeble; 
        }
        // Print out table header
        System.out.format("%" + (maxLength + 4) + "s%25s%25s%5s\n", "Name:", "Used:", "Available:",  "");
        for (Entry<String, Set<String>> d : allDeps.entrySet()) {
            boolean first = true;
            SortedSet<Version> versions = m_versionService.findVersion(d.getKey());
            for (String s : sortVersions(d.getValue())) {
                String name = "";
                String version = "";
                if (!versions.isEmpty()) {
                    version = versions.last().toString();
                }
                // only print name for the first row of the name
                if (first) {
                    name = d.getKey();
                }

                boolean isCurrent = isCurrentVersion(versions, s, version);
                if (!isCurrent || !isArgsCheck("-nolist", args)) {
                    if (!"".equals(name) && !isCurrent) {
                        upgradeble.add(name);
                    }
                    System.out.format("%" + (maxLength + 4) + "s%25s%25s%5s\n", name, s, first ? version : "", !isCurrent ? " <-- upgrade" : "");
                    first = false;
                }
            }
        }
        return upgradeble;
    }

    private boolean isCurrentVersion(SortedSet<Version> versions, String usedVersion, String otherVersion) {
        if ("".equals(otherVersion) || "latest".equals(usedVersion)) {
            // not current as repo version is empty (could also be second line)
            // "latest" is a workspace version so thats ok 
            return true;
        }
        if (usedVersion != null && !versions.isEmpty()) {
            try {
                org.osgi.framework.Version v1 = new org.osgi.framework.Version(usedVersion);
                org.osgi.framework.Version v2 = new org.osgi.framework.Version(versions.last().toString());
                
                if (v1.compareTo(v2) >= 0) {
                    return true;
                }
            } catch (IllegalArgumentException e) {
                // not current
            }
        }
        
        return false;
    }

    private int getMaxLengthOfName(Map<String, Set<String>> allDeps) {
        int maxLength = 0;
        for (String name : allDeps.keySet()) {
            if (name.length() > maxLength) {
                maxLength = name.length();
            }
        }
        return maxLength;
    }
    
    private List<String> sortVersions(Set<String> set){
        List<String> result = new ArrayList<>(set);
        Comparator<String> versionComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1 == null && o2 == null) {
                    // both are equally null thus the same
                    return 0;
                }
                if (o1 == null) {
                    // something is better then null
                    return 1;
                }
                if (o2 == null) {
                    // something is better then null
                    return -1;
                }
                try {
                    org.osgi.framework.Version v1 = new org.osgi.framework.Version(o1);
                    org.osgi.framework.Version v2 = new org.osgi.framework.Version(o2);
                    return v1.compareTo(v2);
                } catch (IllegalArgumentException  e) {
                    // one of the versions was not a correct version number, we will just compare strings
                    return o1.compareTo(o2);
                }
            }
        };
        Collections.sort(result, versionComparator);
        return result;
    }
    
    private void changeDir(File f) {
        changeDir(f.toPath());
    }

    private void changeDir(Path p) {
        sendEvent("org/amdatu/bootstrap/core/BE_QUIET");
        m_navigator.changeDir(p);
        sendEvent("org/amdatu/bootstrap/core/RESET_QUIET");
    }

    private void sendEvent(String topicName) {
        Map<String, Object> props = new HashMap<>();
        Event event = new Event(topicName, props);
        m_eventAdmin.sendEvent(event);
    }

}
