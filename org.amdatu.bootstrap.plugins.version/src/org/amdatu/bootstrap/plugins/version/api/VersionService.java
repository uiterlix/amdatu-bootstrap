package org.amdatu.bootstrap.plugins.version.api;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import aQute.bnd.service.RepositoryPlugin;
import aQute.bnd.version.Version;

public interface VersionService {

    /** 
     * Finds a specific version for a bsn, can return an empty list if the bsn is not found *.
     */
    SortedSet<Version> findVersion(String bsn);

    /** 
     * Return a list of all {@link RepositoryPlugin}'s in the current workspace
     * @return
     */
    List<RepositoryPlugin> getRepositories();
    
    /**
     * Returns a map that holds all bsn's with all version ranges found. This can be executed in a workspace and then it
     * will navigate into all projects and the value will be a list of 1 (or n) version ranges found for the map key 
     * (bsn). This can also be executed in a project and then will return the version ranges in a list of max 1 entry.
     * 
     * This can return a empty {@link Map} if not in workspace or project
     * 
     * @return a map with as key the bsn and as value a list of all known version (ranges).
     */
    Map<String, Set<String>> getAllDependencies();
}
