package org.amdatu.bootstrap.console.impl;

import java.util.Properties;

import org.amdatu.bootstrap.core.api.Prompt;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.gogo.api.CommandSessionListener;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
	
		
		dm.add(createComponent().setInterface(new String[] {Prompt.class.getName(), CommandSessionListener.class.getName()}, null).setImplementation(ConsolePrompt.class));
		
		String[] topics = new String[] {
				"org/amdatu/bootstrap/core/*"
	        };
	        
        Properties props = new Properties();
        props.put(EventConstants.EVENT_TOPIC, topics);
		dm.add(createComponent().setInterface(EventHandler.class.getName(), props).setImplementation(CoreEventHandlers.class));
	}
}
