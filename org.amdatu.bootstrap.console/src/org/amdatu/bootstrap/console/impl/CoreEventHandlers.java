package org.amdatu.bootstrap.console.impl;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

public class CoreEventHandlers implements EventHandler{
    
    public volatile boolean m_quiet = false;

	@Override
	public void handleEvent(Event event) {
		switch(event.getTopic()) {
			case "org/amdatu/bootstrap/core/WORKSPACE_CHANGED":
			    if (!m_quiet) {
			        System.out.println("Workspace changed!");
			    }
			break;
			case "org/amdatu/bootstrap/core/PROJECT_CHANGED": 
			    if (!m_quiet) {
			        System.out.println("Project changed!");
			    }
			break;
			case "org/amdatu/bootstrap/core/BE_QUIET":
			    m_quiet = true;
            break;
			case "org/amdatu/bootstrap/core/RESET_QUIET": 
			    m_quiet = false;
            break;
		}
		
	}

}
