package org.amdatu.bootstrap.plugin.api;

import java.util.Collection;


public interface Plugin {
	String getName();
	String getDescription();
	Collection<Dependency> getDependencies();
	Collection<Dependency> getRunRequirements();
	public void install() throws Exception;
	boolean isInstalled();
	/** Indicates that this {@link Plugin} maybe installed in a workspace. Default is false. */
    boolean isWorkspaceInstallAllowed();
    /** Indicates that this {@link Plugin} maybe installed in a project. Default is true. */
    boolean isProjectInstallAllowed();
}
