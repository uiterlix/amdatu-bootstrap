package org.amdatu.bootstrap.plugin.api;

import java.util.ArrayList;
import java.util.List;

public class Dependency {
	private final String m_bsn;
	private final String m_version;
	
	public Dependency(String bsn) {
		m_bsn = bsn;
		m_version = null;
	}
	
	public Dependency(String bsn, String version) {
		m_bsn = bsn;
		m_version = version;
	}
	
	public String getBsn() {
		return m_bsn;
	}
	
	public String getVersion() {
		return m_version;
	}
	
	public static List<Dependency> fromStrings(String... dependencies) {
		List<Dependency> result = new ArrayList<>();
		for (String coord : dependencies) {
			if(coord.contains(";")) {
				result.add(new Dependency(coord.substring(0, coord.indexOf(";")), coord.substring(coord.indexOf(";" + 1))));
			} else {
				result.add(new Dependency(coord));
			}
		}
		
		return result;
	}

	@Override
	public String toString() {
		return "Dependency [bsn=" + m_bsn + ", version=" + m_version + "]";
	}
}
