package org.amdatu.bootstrap.plugin.api;

import java.util.Collection;
import java.util.Collections;

public abstract class AbstractBasePlugin implements Plugin{
	@Override
	public String getDescription() {
		return "";
	}

	@Override
	public Collection<Dependency> getDependencies() {
		return Collections.emptyList();
	}

	@Override
	public Collection<Dependency> getRunRequirements() {
		return Collections.emptyList();
	}

	@Override
	public void install() throws Exception {
	}
	
	@Override
	public boolean isWorkspaceInstallAllowed() {
	    return false;
	}
	
	@Override
	public boolean isProjectInstallAllowed() {
        return true;
    }

	@Override
	public String toString() {
		return getName();
	}
}
