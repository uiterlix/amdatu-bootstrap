package org.amdatu.bootstrap.plugins.angular;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;

import org.amdatu.bootstrap.core.api.DirectoryStructureBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.amdatu.web.api.WebResourcePlugin;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component(provides=Plugin.class)
public class AngularPlugin extends AbstractBasePlugin{
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile Prompt m_prompt;
	
	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;
	
	@ServiceDependency
	private volatile WebResourcePlugin m_webResourcePlugin;
	
	private volatile BundleContext m_bundleContext;
	
	@Override
	public void install() throws Exception {
		new DirectoryStructureBuilder(m_navigator.getProjectDir(), "static/js/lib");
		
		URL angularFile = m_bundleContext.getBundle().getEntry("/libs/js/angular/angular.min.js");
		URL routeFile = m_bundleContext.getBundle().getEntry("/libs/js/angular/angular-route.min.js");
		
		try(InputStream in = angularFile.openStream()) {
			Files.copy(in, m_navigator.getProjectDir().resolve("static/js/lib/angular.min.js"), StandardCopyOption.REPLACE_EXISTING);
		}
		
		try(InputStream in = routeFile.openStream()) {
			Files.copy(in, m_navigator.getProjectDir().resolve("static/js/lib/angular-route.min.js"), StandardCopyOption.REPLACE_EXISTING);
		}
	}
	
	@Command
	public void seed() throws IOException {
		String appName = m_prompt.askString("What is the name of the app?");
		String viewPath = m_prompt.askString("What is the path of the default view?");
		String view = m_prompt.askString("What is the file name of the default view partial?", viewPath + ".html");
		String controller = m_prompt.askString("What is the name of the controller for the default view?");
		
		TemplateContext context = m_templateEngine.createContext();
		context.put("appName", appName);
		context.put("viewPath", viewPath);
		context.put("view", view);
		context.put("controller", controller);
		
		Files.createDirectories(m_navigator.getProjectDir().resolve("static/js"));
		Files.createDirectories(m_navigator.getProjectDir().resolve("static/partials"));
		
		createFile(context, m_navigator.getProjectDir().resolve("static/js/" + appName +".app.js"), m_bundleContext.getBundle().getEntry("/templates/application.vm"));
		createFile(context, m_navigator.getProjectDir().resolve("static/js/" + appName +".controllers.js"), m_bundleContext.getBundle().getEntry("/templates/controllers.vm"));
		createFile(context, m_navigator.getProjectDir().resolve("static/js/" + appName +".services.js"), m_bundleContext.getBundle().getEntry("/templates/services.vm"));
		createFile(context, m_navigator.getProjectDir().resolve("static/js/" + appName +".directives.js"), m_bundleContext.getBundle().getEntry("/templates/directives.vm"));
		createFile(context, m_navigator.getProjectDir().resolve("static/js/" + appName +".filters.js"), m_bundleContext.getBundle().getEntry("/templates/filters.vm"));
		createFile(context, m_navigator.getProjectDir().resolve("static/partials/" + view), m_bundleContext.getBundle().getEntry("/templates/view.vm"));
		createFile(context, m_navigator.getProjectDir().resolve("static/index.html"), m_bundleContext.getBundle().getEntry("/templates/index.vm"));
		
		String resourcesDir = "static";
		String path = m_prompt.askString("Which path do you want to register on?", resourcesDir);
		String defaultPath = "index.html";

		File bndFile = m_prompt.askChoice("To which bundle do you want to add the handler?", 0, m_navigator.listProjectBndFiles());

		m_webResourcePlugin.addWebResources(path, defaultPath, resourcesDir, bndFile);
	}
	
	private void createFile(TemplateContext context, Path outputFile, URL templateUri) {
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			
			String template = processor.generateString(context);
			try {
				Files.write(outputFile, template.getBytes(), StandardOpenOption.CREATE_NEW);
				System.out.println("Created file: " + outputFile);
			} catch(IOException ex) {
				System.out.println("Skipped file " + outputFile);
			}
		} catch (TemplateException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String getName() {
		return "angular";
	}

	@Override
	public boolean isInstalled() {
		return false;
	}

}
